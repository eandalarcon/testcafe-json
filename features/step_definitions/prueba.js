const {Given, When, Then} = require('cucumber');
const Selector = require('testcafe').Selector;
const globalFunction = require('../support/globalFunctions');
const pruebaPage = require('../support/pages/Prueba.po');

Given('I am open Google\'s search page', async function() {
    await testController.navigateTo(pruebaPage.PruebaPO.url());
});

When('I am typing my search request {string} on Google', async function(text) {
    var textEval = eval("`" + globalFunction.replaceText(text) + "`");
    await testController.typeText(pruebaPage.PruebaPO.searchBox(), textEval);
});

Then('I press the {string} key on Google', async function(keyPress) {
    await this.addScreenshotToReport();
    await testController.pressKey(keyPress);
});

Then('I should see that the first Google\'s result is {string}', async function(text) {
    var textEval = eval("`" + globalFunction.replaceText(text) + "`");
    await this.addScreenshotToReport();
    await testController.expect(pruebaPage.PruebaPO.firstSearchResult().innerText).contains(textEval);
});
