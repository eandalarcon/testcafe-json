const report = require('multiple-cucumber-html-reporter');
const moment = require('moment-timezone');
const path = require('path');
const projectName = process.env.npm_package_name;
const projectVersion = process.env.npm_package_version;
const reportGenerationTime = moment.tz('America/Santiago').format().slice(0, 19).replace('T', ' ');

report.generate({
  reportName: 'Automation Report',
  jsonDir: 'reports',
  reportPath: 'reports',
  openReportInBrowser: true,
  disableLog: true,
  displayDuration: true,
  durationInMS: true,
  customStyle: 'report.css',
  pageFooter: '<p align="right">@Team FIF - Digital Factory</p>',
  metadata:{
    browser: {
      name: 'chrome',
      version: '78.0.3904.108'
    },
    device: 'Local test machine',
    platform: {
        name: 'osx',
        version: '10.14.6'
    }
  },
  customData: {
    title: 'Run info',
    data: [
      { label: 'Project', value: `${projectName}` },
      { label: 'Release', value: `${projectVersion}` },
      { label: 'Report Generation Time', value: `${reportGenerationTime}` }
    ],
  }    
});